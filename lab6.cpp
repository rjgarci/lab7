/*	Ryan Garcia
	rjgarci
        section 002
        Lab 6
        This code asks the user to implement 6 employees and sort the first 5 in alphabetical order.
*/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
        string lastName;
        string firstName;
        int birthYear;
        double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) {
        return rand()%i;
}
int main(int argc, char const *argv[]){
        srand(unsigned (time(0)));
        employee arr[6];
	int i;
        for(i = 0; i < 6; i++){

        cout << "First Name:";
        cin >> arr[i].firstName;
        cout << "Last Name:";
        cin >> arr[i].lastName;
        cout << "Birth Year:";
        cin >> arr[i].birthYear;
        cout << "Hourly Wage:";
        cin >> arr[i].hourlyWage;
        }
        random_shuffle(arr, arr+6, myrandom);
        employee Emp[5];
        for(int i = 0; i < 5; i++){
                Emp[i] = arr[i];
                sort(Emp, Emp + 5, name_order);
        }
        for(int i = 0; i < 5; i++){
                cout << right << setw(5) << (Emp[i].lastName+"," + Emp[i].firstName) << endl;
                cout << right << setw(5) << Emp[i].birthYear << endl;
                cout << right << setw(5) << fixed << setprecision(2) << Emp[i].hourlyWage << endl;
        }


        return 0;
}
bool name_order(const employee& lhs, const employee& rhs){
        return lhs.lastName < rhs.lastName;
}
                      	
